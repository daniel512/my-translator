<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

if($_SERVER['REQUEST_METHOD'] === 'GET') {
    my_translator_get_all_phrases();
}

function my_translator_get_all_phrases() {
    header('Content-Type: application/json');

    $dbHandler = new My_Translator_Db_Handler();

    echo json_encode($dbHandler->get_all_phrases());
}

