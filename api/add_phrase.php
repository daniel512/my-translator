<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

header('Access-Control-Allow-Origin: *');

$data = json_decode(file_get_contents('php://input'));

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $dbHandler = new My_Translator_Db_Handler();

    $dbHandler->insert(new My_Translator_Phrase($data->en, $data->pl));
}