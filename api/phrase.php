<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

if($_SERVER['REQUEST_METHOD'] === 'GET') {
    my_translator_get_phrases();
}

function my_translator_get_phrases() {
    header('Content-Type: application/json');

    $phrase = new My_Translator_Phrase($_GET['en'], $_GET['pl']);

    $dbHandler = new My_Translator_Db_Handler();

    echo json_encode($dbHandler->get_single_phrase($phrase));
}

