<?php
/**
 * Plugin Name: My Translator
 * Description: Plugin for multi languages page
 */

require_once __DIR__ . '/includes/My_Translator_Db_Handler.php';
require_once __DIR__ . '/includes/My_Translator_Phrase.php';
require_once __DIR__ . '/includes/My_Translator.php';

function my_translator_add_button($wp_admin_bar)
{
    $args = array(
        'id' => 'my_translator_btn',
        'title' => 'Translate Page',
        'meta' => array(
            'class' => 'my_translator_btn',
            'title' => 'Transate Page'
        )
    );
    $wp_admin_bar->add_node($args);
}

add_action('admin_bar_menu', 'my_translator_add_button', 998);

function my_translator_activate()
{
    $db_handler = new My_Translator_Db_Handler();
    $db_handler->create_db();
}

register_activation_hook(__FILE__, "my_translator_activate");

function my_translator_register_js()
{
    wp_register_script(
        'script',
        plugins_url('/js/main.js', __FILE__)
    );

    wp_enqueue_script('extensions', plugins_url('/js/extensions.js', __FILE__));
    wp_enqueue_script('script',);

    $script_params = array(
        'url' => plugin_dir_url(__FILE__),
    );

    wp_localize_script('script', 'scriptParams', $script_params);
}

add_action('wp_enqueue_scripts', 'my_translator_register_js', 999);

function my_translator_load_plugin()
{
    $translator = new My_Translator();
    add_action('wp_loaded', array($translator, 'init'));
}

if (isset($_GET['lang']) && $_GET['lang'] === 'pl') {
    add_action('plugins_loaded', 'my_translator_load_plugin');
}

