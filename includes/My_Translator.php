<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

class My_Translator
{

    public function init(): void
    {
        add_filter('the_content', array($this, 'translate'));
        add_filter('the_title', array($this, 'translate'));
    }

    public function translate(string $sourceText): string
    {
        try {
            foreach ($this->get_data() as $phrase) {
                if ($this->containsSentence($sourceText, $phrase->en)) {
                    $sourceText = str_replace($phrase->en, $phrase->pl, $sourceText);
                }
            }
            return $sourceText;
        } catch (Exception $e) {
            return $sourceText;
        }
    }

    private function containsSentence(string $text, string $needle): bool
    {
        return preg_match('/' . $needle . '/', $text);
    }

    private function get_data(): array
    {
        $dbHandler = new My_Translator_Db_Handler();
        return $dbHandler->get_all_phrases();
    }
}
