<?php

class My_Translator_Db_Handler
{

    public function insert(My_Translator_Phrase $phrase): void
    {
        global $wpdb;

        if (!$this->phrase_exists($phrase)) {
            $sql = "INSERT INTO mytranslator_phrases(en, pl) VALUES('$phrase->en', '$phrase->pl')";
        } else {
            $sql = "UPDATE mytranslator_phrases SET en = '$phrase->en', pl = '$phrase->pl' WHERE en LIKE '$phrase->en'";
        }

        $wpdb->query($sql);
    }

    public function get_all_phrases(): array
    {
        global $wpdb;

        $sql = "SELECT * FROM mytranslator_phrases";

        return $wpdb->get_results($sql);
    }

    public function get_single_phrase(My_Translator_Phrase $phrase): ?My_Translator_Phrase
    {
        global $wpdb;

        $sql = "SELECT * FROM mytranslator_phrases WHERE en LIKE '$phrase->en' OR pl LIKE '$phrase->pl'";

        $result = $wpdb->get_results($sql);

        return sizeof($result) > 0 ? new My_Translator_Phrase($result[0]->en, $result[0]->pl) : null;
    }

    public function create_db(): void
    {
        global $wpdb;

        $phrasesTable = 'mytranslator_' . 'phrases';

        if ($wpdb->get_var("show tables like '$phrasesTable'") != $phrasesTable) {

            $sql = "CREATE TABLE $phrasesTable (";
            $sql .= "id int(11) NOT NULL auto_increment, ";
            $sql .= "en varchar(2000), ";
            $sql .= "pl varchar(2000), ";
            $sql .= "primary key(id) )";
        }

        require_once(ABSPATH . '/wp-admin/includes/upgrade.php');

        $wpdb->query($sql);
    }

    private function phrase_exists(My_Translator_Phrase $phrase): bool
    {
        global $wpdb;

        $sql = "SELECT * FROM mytranslator_phrases WHERE en LIKE '$phrase->en'";

        return sizeof($wpdb->get_results($sql)) > 0;
    }
}