<?php

class My_Translator_Phrase
{

    public string $en;
    public string $pl;

    function __construct(string $en, string $pl)
    {
        $this->en = $en;
        $this->pl = $pl;
    }
}
