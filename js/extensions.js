HTMLElement.prototype.setStyle = function (styleSet) {
    Object.entries(styleSet).forEach(it => eval(`this.style.${it[0]} =  '${it[1]}'`))

    return this
}

HTMLElement.prototype.setAttributes = function(attributes) {
    Object.entries(attributes).forEach(it => this.setAttribute(it[0], `${it[1]}`))

    return this
}

HTMLElement.prototype.render = function (parent = document.body) {
    parent.appendChild(this)
}

HTMLElement.prototype.addChildren = function (...children) {
    children.forEach(it => this.appendChild(it))

    return this
}

HTMLElement.prototype.setInnerHTML = function (html) {
    this.innerHTML = html

    return this
}

HTMLElement.prototype.setInnerText = function (text) {
    this.innerText = text

    return this
}

HTMLElement.prototype.onEvent = function (event, func) {
    this.addEventListener(event, func)

    return this
}

HTMLElement.prototype.setText = function (text) {
    this.innerHTML = this.innerHTML.replace(this.innerText, text)

    return this
}

String.prototype.create = function() {
    return document.createElement(this)
}

Array.prototype.isEmpty = function() {
    return this.length <= 0
}

Array.prototype.isNotEmpty = function() {
    return !this.isEmpty()
}

Array.prototype.last = function() {
    return this[this.length - 1]
}

Array.prototype.first = function() {
    return this[0]
}
