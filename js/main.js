const url = scriptParams.url

function getAllTextTags() {
    return Array.from(document.querySelectorAll('p'))
        .concat(Array.from(document.querySelectorAll('h1')))
        .concat(Array.from(document.querySelectorAll('h2')))
        .concat(Array.from(document.querySelectorAll('h3')))
        .concat(Array.from(document.querySelectorAll('h4')))
        .concat(Array.from(document.querySelectorAll('h5')))
        .filter(it => it.innerText !== '')
}

window.addEventListener('load', async () => {
    try {
        document.querySelector('.my_translator_btn').addEventListener('click', initTranslation)
    } catch (e) {
    }
    if (currentLanguage() === 'pl') {
        await translate()
    }
    languageSwitcher()
})

function changeLanguage(language = null) {
    if (language !== null && window.location.href.includes(`lang=${language}`))
        return

    window.location.href = window.location.href.replace(new RegExp('(\\??lang=\\w\\w)'), '') + `?lang=${language}`
}

async function initTranslation() {
    if (currentLanguage() !== 'en') {
        changeLanguage('en')
    }

    const phrases = Array.from(await getAllPhrases())

    getAllTextTags().forEach(it => {
        const translation = phrases.filter(phrase => phrase.en === it.innerText)
        it.setStyle({outline: translation.isEmpty() ? '1px solid red' : '1px solid blue'})

        it.onEvent('click', () => {
            try {
                document.getElementById('my_translator_form').remove()
            } catch (e) {
            }
            'div'.create()
                .setAttributes({id: 'my_translator_form'})
                .setStyle({
                    background: 'white',
                    padding: '20px',
                    position: 'fixed',
                    top: '25%',
                    left: '20%',
                    width: '60%',
                    border: '2px solid black'
                }).addChildren(
                textAreaEn.setInnerText(it.innerText),
                translation.isNotEmpty() ? textAreaPl.setInnerText(translation.last().pl) : textAreaPl.setInnerText(''),
                submit,
                closeBtn
            ).render()
        })
    })
}

function currentLanguage() {
    const url = window.location.href
    const urlParams = url.slice(url.indexOf('lang'))
    const langParam = urlParams.includes('&') ? urlParams.substring(0, urlParams.indexOf('&')) : urlParams

    return langParam.slice(langParam.indexOf('=')).replace('=', '')
}

async function getTranslation(en = '', pl = '') {
    const params = new URLSearchParams({en: en, pl: pl})
    const response = await fetch(`${url}/api/phrase.php?${params}`)
    return await response.json()
}

async function translationExists(en, pl) {
    return await getTranslation(en, pl) !== null
}

async function translate() {
    await getAllPhrases().then(data => data.forEach(it => {
        getAllTextTags().forEach(tag => {
            if (tag.innerText === it.en)
                tag.setText(it.pl)
        })
    }))
}

async function getAllPhrases() {
    const response = await fetch(`${url}/api/phrases.php`)
    return await response.json()
}

const closeBtn = 'div'.create().setStyle({
    position: 'absolute',
    top: '2px',
    right: '5px',
    cursor: 'pointer',
}).setInnerText('x')
    .onEvent('click', () => {
        document.getElementById('my_translator_form').remove()
    })

const submit = 'input'.create().setAttributes({type: 'submit'}).setStyle({fontSize: '13px'}).onEvent('click', (e) => {
    e.preventDefault()
    fetch(`${url}/api/add_phrase.php`, {
        method: "post",
        header: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
        body: JSON.stringify({
            en: document.getElementById('my_translator_form').children[0].value,
            pl: document.getElementById('my_translator_form').children[1].value
        })
    })
        .then(response => afterTranslationDialog(response.status))
})

const textAreaEn = 'textarea'.create().setAttributes({
    type: 'text',
    placeholder: 'en',
    name: 'en'
}).setStyle({margin: '10px 0', background: 'silver'}).setAttributes({disabled: true})

const textAreaPl = 'textarea'.create().setAttributes({
    type: 'text',
    placeholder: 'pl',
    name: 'pl'
}).setStyle({margin: '10px 0'})

function languageSwitcher() {
    const btnBase = 'div'.create().setStyle({
        width: '50%',
        color: 'silver',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '15px',
        textTransform: 'uppercase',
        fontWeight: 'bold'
    })

    'div'.create().setStyle({
        display: 'flex',
        position: 'fixed',
        bottom: '0px',
        right: '50px',
        width: '80px',
        height: '35px',
        backgroundColor: 'black',
        cursor: 'pointer',
        zIndex: 99999
    }).addChildren(
        btnBase.setStyle({borderRight: '1px solid silver'}).setInnerText('pl').onEvent('click', () => changeLanguage('pl')),
        btnBase.cloneNode(true).setInnerText('en').onEvent('click', () => changeLanguage('en')),
    ).render()
}

function afterTranslationDialog(statusCode) {
    const message = statusCode === 200 ? "Translation updated" : "Error occured"
    window.alert(message)
}
